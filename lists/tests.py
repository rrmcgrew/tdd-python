from django.urls import resolve
from django.test import TestCase

from lists.models import Item, List
from lists.views import home_page

# Create your tests here.
class HomePageTest(TestCase):
    def test_home_page_returns_correct_html(self):
        response = self.client.get('/')        
        self.assertTemplateUsed(response, 'home.html')

class ListViewTest(TestCase):
    def test_uses_list_template(self):
        list_ = List.objects.create()
        response = self.client.get(f'/lists/{list_.id}')
        self.assertTemplateUsed(response, 'list.html')

    def test_displays_only_items_for_that_list(self):
        correct_list = List.objects.create()
        wrong_list = List.objects.create()
        item1 = Item.objects.create(text='item 1', list=correct_list)
        item2 = Item.objects.create(text='item 2', list=correct_list)
        item3 = Item.objects.create(text='item 3', list=wrong_list)
        item4 = Item.objects.create(text='item 4', list=wrong_list)
        response = self.client.get(f'/lists/{correct_list.id}')
        self.assertContains(response, item1.text)
        self.assertContains(response, item2.text)
        self.assertNotContains(response, item3.text)
        self.assertNotContains(response, item4.text)
    
    def test_passes_list_id_to_template(self):
        other_list = List.objects.create()
        list_ = List.objects.create()
        response = self.client.get(f'/lists/{list_.id}')
        self.assertEqual(response.context['list'], list_)

class ListAndItemModelTest(TestCase):
    def test_saving_and_retrieving_items(self):
        list_ = List()
        list_.save()
        first_item = Item()
        first_item.text = 'first item'
        first_item.list = list_
        first_item.save()
        second_item = Item()
        second_item.text = 'second item'
        second_item.list = list_
        second_item.save()
        saved_list = List.objects.first()
        saved_items = Item.objects.all()
        self.assertEqual(saved_list, list_)
        self.assertEqual(saved_items.count(), 2)
        self.assertEqual(saved_items[0].text, 'first item')
        self.assertEqual(saved_items[0].list, list_)
        self.assertEqual(saved_items[1].text, 'second item')
        self.assertEqual(saved_items[1].list, list_)

class NewListTest(TestCase):
    def test_can_save_POST_request(self):
        data = { 'item_text': 'A new list item' }
        response = self.client.post('/lists/new', data)
        new_item = Item.objects.first()
        self.assertEqual(Item.objects.count(), 1)
        self.assertEqual(new_item.text, 'A new list item')

    def test_redirects_after_POST(self):
        data = { 'item_text': 'A new list item' }
        response = self.client.post('/lists/new', data=data)
        list_ = List.objects.first()
        self.assertRedirects(response, f'/lists/{list_.id}')

class NewItemTest(TestCase):
    def test_can_save_POST_request_to_existing_list(self):
        wrong_list = List.objects.create()
        list_ = List.objects.create()
        self.client.post(
            f'/lists/{list_.id}/add_item',
            data={'item_text': 'new item'}
        )
        new_item = Item.objects.first()
        self.assertEqual(Item.objects.count(), 1)
        self.assertEqual(new_item.text, 'new item')
        self.assertEqual(new_item.list, list_)

    def test_redirects_to_list_view(self):
        wrong_list = List.objects.create()
        list_ = List.objects.create()
        response = self.client.post(
            f'/lists/{list_.id}/add_item',
            data={'item_text': 'new item'}
        )
        self.assertRedirects(response, f'/lists/{list_.id}')
