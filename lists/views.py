from django.shortcuts import redirect, render

from .models import Item, List

def home_page(request):
    new_item_text = ''
    template = 'home.html'
    return render(request, template)

def view_list(request, list_id):
    list_ = List.objects.get(id=list_id)
    template = 'list.html'
    data = { 'list': list_}
    return render(request, template, data)

def add_item(request, list_id):
    list_ = List.objects.get(id=list_id)
    Item.objects.create(text=request.POST.get('item_text', ''), list=list_)
    return redirect(f'/lists/{list_.id}')

def new_list(request):
    list_ = List.objects.create()
    return add_item(request, list_.id)