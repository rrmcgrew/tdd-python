#!/usr/bin/env python
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.common.keys import Keys
import os
import time

class NewVisitorTest(StaticLiveServerTestCase):
    MAX_WAIT = 10

    def setUp(self):
        self.browser = webdriver.Firefox()
        staging_server = os.environ.get('STAGING_SERVER')
        if staging_server:
            self.live_server_url = 'http://' + staging_server
    
    def tearDown(self):
        self.browser.quit()

    def wait_for_row_in_list_table(self, row_text):
        start_time = time.time()
        while True:
            try:
                table = self.browser.find_element_by_id('id_list_table')
                rows = table.find_elements_by_tag_name('tr')
                self.assertIn(row_text, [row.text for row in rows])
                return
            except (AssertionError, WebDriverException) as e:
                if time.time() - start_time > self.MAX_WAIT:
                    raise e
                time.sleep(0.5)

    def check_for_row_in_table(self, row_text):
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('tr')
        self.assertIn(row_text, [row.text for row in rows])
    
    def test_can_start_list_for_one_user(self):
        # User loads home page.
        self.browser.get(self.live_server_url)

        # User notes page title and header mentions to-do lists
        assert 'To-Do lists' in self.browser.title
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('To-Do', header_text)

        # User invited to enter an item straight away
        inputbox = self.browser.find_element_by_id('id_new_item')
        self.assertEqual(
            inputbox.get_attribute('placeholder'),
            'Enter a to-do item'
        )

        # User enters 'Steal underpants'
        inputbox.send_keys('Steal underpants')

        # Upon hitting Enter, page updates and lists '1: Steal underpants' as an item
        inputbox.send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table('1: Steal underpants')

        # There is still a text box inviting user to add another item.
        # User enters '???'
        inputbox = self.browser.find_element_by_id('id_new_item')
        inputbox.send_keys('???')
        inputbox.send_keys(Keys.ENTER)

        # Page updates again, now showing two items.
        self.wait_for_row_in_list_table('1: Steal underpants')
        self.wait_for_row_in_list_table('2: ???')

    def test_multiple_users_can_start_lists_at_different_urls(self):
        # User A starts a new list
        self.browser.get(self.live_server_url)
        inputbox = self.browser.find_element_by_id('id_new_item')
        inputbox.send_keys('Steal underpants')
        inputbox.send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table('1: Steal underpants')

        # User A notes list has unique URL
        user_a_list_url = self.browser.current_url
        self.assertRegex(user_a_list_url, '/lists/.+')

        # User B enters the site; there is no trace of User A's list
        self.browser.quit()
        self.browser = webdriver.Firefox()
        self.browser.get(self.live_server_url)
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Steal underpants', page_text)

        # User B starts a new list by entering an item
        inputbox = self.browser.find_element_by_id('id_new_item')
        inputbox.send_keys('Buy milk')
        inputbox.send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table('1: Buy milk')

        # User B's list has its own unique URL
        user_b_list_url = self.browser.current_url
        self.assertRegex(user_b_list_url, '/lists/.+')
        self.assertNotEqual(user_b_list_url, user_a_list_url)

        # Still no trace of User A's list
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Steal underpants', page_text)
        self.assertIn('Buy milk', page_text)
    
    def test_layout_and_styling(self):
        # User goes to the home page
        self.browser.get(self.live_server_url)
        self.browser.set_window_size(1024, 768)

        # Input text box is centered
        inputbox = self.browser.find_element_by_id('id_new_item')
        self.assertAlmostEqual(
            inputbox.location['x'] + inputbox.size['width'] / 2,
            512,
            delta=10
        )

        # User starts new list and input box remains centered
        inputbox.send_keys('item')
        inputbox.send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table('1: item')
        inputbox = self.browser.find_element_by_id('id_new_item')
        self.assertAlmostEqual(
            inputbox.location['x'] + inputbox.size['width'] / 2,
            512,
            delta=10
        )
